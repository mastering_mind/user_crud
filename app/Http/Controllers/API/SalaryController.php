<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\WorkedHour;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Validation\Validator; // Добавляем пространство имен Validator

class SalaryController extends Controller
{
    public function createEmployee(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email|unique:employees,email',
            'password' => 'required|min:6',
            'hourly_rate' => 'required|numeric|min:1',
        ],
            [
                'email.required' => 'Поле email является обязательным',
                'email.email' => 'Email должен быть действительным адресом электронной почты',
                'email.unique' => 'Данный email уже зарегистрирован',
                'password.required' => 'Поле пароль является обязательным',
                'password.min' => 'Пароль должен содержать минимум :min символов',
                'hourly_rate.required' => 'Поле hourly_rate является обязательным',
                'hourly_rate.numeric' => 'Поле hourly_rate должно быть численным',
                'hourly_rate.min' => 'Часовой рейт должен содержать минимум :min символов',
            ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        $employee = Employee::create($validatedData);

        return response()->json($employee, 201);
    }

    public function addWorkedHours(Request $request)
    {
        $validatedData = $request->validate([
            'employee_id' => 'required|exists:employees,id',
            'hours_worked' => 'required|numeric|min:1',
        ],
            [
                'employee_id.required' => 'Поле employee_id является обязательным',
                'employee_id.exists' => 'Указанный сотрудник не существует',
                'hours_worked.required' => 'Поле hours_worked является обязательным',
                'hours_worked.numeric' => 'Поле hours_worked должно быть численным',
                'hours_worked.min' => 'Количество часов должно содержать минимум :min символов',
            ]);

        $employee = Employee::findOrFail($request->employee_id);
        $workedHours = WorkedHour::create($validatedData);

        return response()->json($workedHours, 201);
    }

    public function unpaidSalaries()
    {
        $unpaidSalaries = [];
        $employees = Employee::all();

        foreach ($employees as $employee) {
            $totalHours = $employee->workedHours()->sum('hours_worked');
            $totalSalary = $totalHours * $employee->hourly_rate;
            $unpaidSalaries[] = [
                'employee_id' => $employee->id,
                'total_salary' => $totalSalary,
            ];
        }

        return response()->json($unpaidSalaries);
    }

    public function paySalaries()
    {
        $response = $this->unpaidSalaries();

        if ($response->status() == 200) {

            $unpaidSalaries = $response->getData();
            foreach ($unpaidSalaries as $unpaidSalary) {
                $employeeId = $unpaidSalary->employee_id;
                $totalSalary = $unpaidSalary->total_salary;

                $payment = new Payment();
                $payment->employee_id = $employeeId;
                $payment->amount = $totalSalary;
                $payment->save();

                $employee = Employee::findOrFail($employeeId);
                $employee->workedHours()->update(['paid' => true]);
            }

            return response()->json(['message' => 'Salaries paid successfully']);
        }
        else {
            return response()->json(['error' => 'Failed to retrieve unpaid salaries'], 500);
        }
    }

    protected function failedValidation(Validator $validator): \Illuminate\Http\JsonResponse
    {
        return response()->json($validator->errors(), 422);
    }
}
