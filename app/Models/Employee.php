<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'email', 'password', 'hourly_rate',
    ];

    protected $hidden = [
        'password',
    ];

    public function workedHours(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(WorkedHour::class);
    }
}
