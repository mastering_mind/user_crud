<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkedHour extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id', 'hours_worked', 'paid'
    ];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
