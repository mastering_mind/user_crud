<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\SalaryController;

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:sanctum');



Route::post('/employees', [SalaryController::class, 'createEmployee']);
Route::post('/worked-hours', [SalaryController::class, 'addWorkedHours']);
Route::get('/unpaid-salaries', [SalaryController::class, 'unpaidSalaries']);
Route::post('/pay-salaries', [SalaryController::class, 'paySalaries']);
