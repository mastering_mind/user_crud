<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\Payment;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SalaryControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_create_an_employee(){
        $data = [
            'email' => 'test@example.com',
            'password' => 'password',
            'hourly_rate' => 15.0,
        ];

        $response = $this->postJson('/api/employees', $data);

        $response->assertStatus(201)
            ->assertJson(['email' => 'test@example.com']);
    }

    public function it_can_add_worked_hours_for_an_employee(){
        $employee = Employee::factory()->create();

        $data = [
            'hours' => 8,
            'employee_id' => $employee->id
        ];

        $response = $this->postJson("/api/worked-hours", $data);

        $response->assertStatus(201)
            ->assertJson(['hours_worked' => 8]);
    }

    public function it_can_get_unpaid_salaries() {
        $employee = Employee::factory()->create();

        Payment::factory()->create(['employee_id' => $employee->id, 'amount' => 100]);
        Payment::factory()->create(['employee_id' => $employee->id, 'amount' => 150]);

        $response = $this->getJson('/api/unpaid-salaries');

        $response->assertStatus(200);

        $response->assertJsonCount(2);

        $response->assertJsonFragment(['employee_id' => $employee->id, 'total_salary' => 250]);
    }

    public function it_can_pay_salaries() {
        $employee1 = Employee::factory()->create();
        $employee2 = Employee::factory()->create();

        Payment::create([
            'employee_id' => $employee1->id,
            'amount' => 100.00,
        ]);

        Payment::create([
            'employee_id' => $employee2->id,
            'amount' => 150.00,
        ]);

        $this->assertCount(2, Payment::all());

        $response = $this->postJson('/api/pay-salaries');

        $response->assertStatus(200)
            ->assertJson(['message' => 'Salaries paid successfully']);

        $this->assertCount(0, Payment::all());
    }
}

